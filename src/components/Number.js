import { Component } from "react";

class Number extends Component{
  constructor(props){
    super(props)
    this.state = {number : 0}
  }
  render(){
    return(
    <div>
        <button onClick={()=>this.setState({number : this.state.number +1})}>Increase</button>
        <button onClick={()=>this.setState({number : this.state.number -1})}>Decrease</button>
      <p>{this.state.number}</p>
    </div>)
  }
}
export default Number